# Code origiennellement par @Thibault Neveu - https://github.com/thibo73800
# Modifié par Claire Perrot - janvier 2021


import numpy as np
from random import randint
import random
import time


class EnvGrid(object):

    def __init__(self):
        super(EnvGrid, self).__init__()
        self.wall = -2000
        self.grid = [
            [-0.1, -0.1, -0.1, -0.1,-0.1, -0.1, -0.1, -0.1,-0.1, -0.1, -0.1],
            [-0.1, self.wall, self.wall, self.wall,self.wall, self.wall, -0.1, self.wall,self.wall, self.wall, -0.1],
            [-0.1, -0.1, -0.1, -0.1,-0.1, -0.1, -0.1, self.wall,-0.1, -0.1, -0.1],
            [-0.1, -0.1, -0.1, self.wall,-0.1, self.wall, -0.1, self.wall,-0.1, -0.1, self.wall],
            [-0.1, -0.1, -0.1, self.wall,-0.1, self.wall, -0.1, -0.1,-0.1, -0.1, 2000],
            [-0.1, self.wall, -0.1, -0.1,-0.1, self.wall, self.wall, self.wall,-0.1, -0.1, -0.1],
            [-0.1, self.wall, -0.1, -0.1,-0.1, -0.1, -0.1, -0.1,-0.1, -0.1, -0.1],
            [-0.1, self.wall, -0.1, -0.1,self.wall, -0.1, -0.1, -0.1,-0.1, -0.1, -0.1],
            [-0.1, self.wall, -0.1, -0.1,-0.1, -0.1, -0.1, -0.1,-0.1, -0.1, -0.1],
            [-0.1, self.wall, -0.1, -0.1,-0.1, -0.1, -0.1, -0.1,-0.1, -0.1, self.wall],
            [-0.1, -0.1, -0.1, self.wall,-0.1, -0.1, -0.1, self.wall,-0.1, -0.1, -0.1],
        ]

        # starting position
        self.st_pos = [0, 0]
        self.treasure_pos = [10, 4]
        self.exit_pos = [6, 10]
        self.goals_achieved_count = 0
        self.has_found_treasure = False
        self.has_found_exit = False
        self.maze_ended = False
        self.reset()

        self.actions = [
            [-1, 0],  # Up
            [1, 0],  # Down
            [0, -1],  # Left
            [0, 1]  # Right
        ]

    def get_state(self):
        if self.has_found_treasure:
            state = self.y * len(self.grid) + self.x + 1 + (len(self.grid)*len(self.grid))
        else:
            state = self.y * len(self.grid) + self.x + 1
        print("Get State : ", state)
        return state

    def reset(self):
        """
            Reset du jeu
        """
        self.x = self.st_pos[0]
        self.y = self.st_pos[1]
        self.goals_achieved_count = 0
        self.has_found_treasure = False
        self.has_found_exit = False
        self.maze_ended = False
        self.grid[self.treasure_pos[1]][self.treasure_pos[0]] = 2000
        self.grid[self.exit_pos[1]][self.exit_pos[0]] = -0.1

    def step(self, action):
        """
            Action: 0, 1, 2, 3;
            mise a jour de la position + retourne l'état d'arrivée et la récompense
        """
        self.y = max(0, min(self.y + self.actions[action][0], len(self.grid) - 1))
        self.x = max(0, min(self.x + self.actions[action][1], len(self.grid) - 1))

        return self.get_state(), self.grid[self.y][self.x]

    def show(self):
        """
            # Show the grid
        """
        print("---------------------")
        y = 0
        for line in self.grid:
            x = 0
            for pt in line:
                print("%s\t" % (pt if y != self.y or x != self.x else "X"), end="")
                x += 1
            y += 1
            print("")

    def is_finished(self):
        return self.maze_ended == True

    def goal_achieved(self):
        return self.grid[self.y][self.x] == 2000

    def update_rewards_grid(self):
        self.grid[self.treasure_pos[1]][self.treasure_pos[0]] = -0.1
        self.grid[self.exit_pos[1]][self.exit_pos[0]] = 2000

    def print_game_state_informations(self):
        print("goals achieved count : \t", self.goals_achieved_count)
        print("Has found treasure ? \t", self.has_found_treasure)
        print("Has found exit ? \t", self.has_found_exit)
        self.show()

    def update_game(self):
        if self.goal_achieved() and self.goals_achieved_count == 0:
            self.goals_achieved_count += 1
            self.has_found_treasure = True
            self.update_rewards_grid()
            self.print_game_state_informations()

        elif self.goal_achieved() and self.goals_achieved_count == 1:
            self.goals_achieved_count += 1
            self.has_found_exit = True
            self.print_game_state_informations()

        elif self.goal_achieved() and self.goals_achieved_count == 2:
            self.maze_ended = True


def take_action(st, Q, eps):
    # Choisir une action (retourne l'action choisie)
    if random.uniform(0, 1) < eps:  # exploration
        action = randint(0, 3)
    else:  # exploitation
        action = np.argmax(Q[st])
    return action


if __name__ == '__main__':
    start_time = time.time()
    env = EnvGrid()
    env.reset()
    st = env.get_state()
    nbr_training = 1000
    grid_length = len(env.grid)
    actions_length = len(env.actions)

    # Creation de la q-table en fonction de la taille de la grille et du nombre d'actions possibles
    Q = ((grid_length*grid_length) * 2 + 1)*[[0]]
    for i in range(len(Q)):
        Q[i] = actions_length*[0]

    # affichage de la Q-table initiale
    print('------- Q-table initiale -------')
    print('--------------------------------')
    print('     up     down    left   right')
    for s in range(1, len(Q)):
        formatted_Q = ['%.2f' % elem for elem in Q[s]]
        print(s, formatted_Q)

    for _ in range(nbr_training):
        # Reset
        env.reset()
        st = env.get_state()
        # while not env.is_finished():
        while not env.is_finished():
            env.show()
            action = take_action(st, Q, 0.8)
            stp1, r = env.step(action)
            atp1 = take_action(stp1, Q, 0.0)
            Q[st][action] = Q[st][action] + 0.1 * \
                (r + 0.9*Q[stp1][atp1] - Q[st][action])
            st = stp1
            if env.goal_achieved():
                env.update_game()
        print("----\t",nbr_training," entrainement(s) finis en : ",(time.time() - start_time),"\t----")
        print("Training done ? \t", env.is_finished())

    # affichage de la Q-table finale
    print('     up     down    left   right')
    for s in range(1, len(Q)):
        formatted_Q = ['%.2f' % elem for elem in Q[s]]
        print(s, formatted_Q)

    # affichage de la policy finale apprise et du nombre d'étapes
    env.reset()
    s = env.get_state()
    print("Etat de départ", s)
    nb_steps_to_get_the_treasure = 0
    nb_steps_to_get_out = 0
    nb_steps = 0
    while not env.is_finished():
        a = take_action(s, Q, 0.0)
        next_state, reward = env.step(a)
        s = next_state
        if env.goals_achieved_count < 1:
            print("Recherche le trésor, état : ", next_state)
            nb_steps_to_get_the_treasure += 1
        elif env.goals_achieved_count >= 1 and env.goals_achieved_count < 2:
            print("Recherche la sortie, état : ", next_state)
            nb_steps_to_get_out += 1

        if env.goal_achieved():
            if env.goals_achieved_count < 1:
                print("A trouvé le trésor en ",
                      nb_steps_to_get_the_treasure, " etapes")
            elif env.goals_achieved_count >= 1 and env.goals_achieved_count < 2:
                print("A trouvé la sortie en ", nb_steps_to_get_out, " etapes")
            env.update_game()

    print("A terminé le labyrinthe en ",
          (nb_steps_to_get_the_treasure + nb_steps_to_get_out), " etapes et ", (time.time() - start_time), "sec.")
