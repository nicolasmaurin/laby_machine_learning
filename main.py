import numpy as np
from random import randint
import random


class Grid(object):
    def __init__(self):

        self.grid = [
            [0,0,0,0,0,0,0,0,0,0],
            [0,-1,-1,-1,0,-1,-1,0,0,0],
            [0,0,0,0,0,-1,0,0,0,0],
            [0,0,-1,0,-1,0,0,0,0,-1],
            [0,0,-1,0,-1,0,0,0,0,+20],
            [0,-1,0,0,0,0,0,0,0,0],
            [0,-1,0,-1,-1,0,-1,-1,0,0],
            [0,-1,0,0,0,0,0,0,0,0],
            [0,-1,0,0,0,0,0,0,0,-1],
            [0,0,0,-1,0,0,+10,-1,0,0],
        ]

        self.start_pos = [0,0]
        self.reset()

        self.actions = [
            [-1, 0],  # Up
            [1, 0],  # Down
            [0, -1],  # Left
            [0, 1]  # Right
        ]

    def reset(self):
        self.x = self.start_pos = [0]
        self.y = self.start_pos = [0]

if __name__ == '__main__':

